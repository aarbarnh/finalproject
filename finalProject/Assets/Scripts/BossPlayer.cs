﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPlayer : MonoBehaviour
{
    //player variables for boss level
    private Transform tf;
    public float moveSpeed = 5.0f;
    public float moveNoise = 5.0f;
    public float turnSpeed = 50.0f;
    public float turnNoise = 5.0f;
    public NoiseMaker noise;

    //audio clips, source
    public AudioSource Magic;
    public AudioClip effect;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        noise = GetComponent<NoiseMaker>();
        Magic.clip = effect;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) //movement, tank controls
        {
            MoveForward();
        }
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            MoveBackward();
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            RotateLeft();
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            RotateRight();
        }

        if (Input.GetKey(KeyCode.Space) && Time.time > BossManager.boss.nextFire) //fireball ranged 
        {
            BossManager.boss.nextFire = Time.time + BossManager.boss.fireRate;
            Magic.Play();
            GameObject fireProject = Instantiate<GameObject>(BossManager.boss.fireball, BossManager.boss.fireSpawn.position, BossManager.boss.fireSpawn.rotation);
        }

        if (Input.GetKeyDown(KeyCode.R) && Time.time > BossManager.boss.attackNext) //melee attack
        {
            BossManager.boss.attackNext = Time.time + BossManager.boss.attackRate;
            BossManager.boss.sword.gameObject.SetActive(true);
        }
    }

    void MoveForward() //move forward
    {
        tf.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }
    void MoveBackward() //move backward
    {
        tf.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }
    void RotateLeft() //rotate left
    {
        tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, turnNoise);
        }
    }
    void RotateRight() //rotate right
    {
        tf.Rotate(0, 0, -(turnSpeed * Time.deltaTime));
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, turnNoise);
        }
    }

    void OnTriggerEnter2D(Collider2D other) //if hit by boss fireball
    {
        if (other.gameObject.CompareTag("EBullet"))
        {
            BossManager.boss.playerHP -= BossManager.boss.bossAtkVal;
            UIManager.ui.health.text = BossManager.boss.playerHP.ToString() + "/50"; //update ui
            Destroy(other.gameObject);
        }
    }
}

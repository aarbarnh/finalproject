﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{
    public float currentVolume = 0; //player volume
    public float volumeDecay = 0.015f; //decay rate

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentVolume > 0) //if player isn't moving decay will happen
        {
            currentVolume -= volumeDecay; 
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonController : MonoBehaviour
{
    private AISenses senses; //grab senses
    private Transform tf; //grab transform

    //dragon variables
    public float turnSpeed = 30.0f;

    //audio clip/source
    public AudioSource Fire;
    public AudioClip effect;

    // Start is called before the first frame update
    void Start()
    {
        senses = GetComponent<AISenses>();
        tf = GetComponent<Transform>();
        Fire.clip = effect;
    }

    // Update is called once per frame
    void Update()
    {
        if(senses.HearPlayer(BossManager.boss.player)) //if boss hears, rotate and fire magic
        {
            LookFor();
            MagicAttack();
        }
    }

    public void LookFor() //rotates 
    {
        tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
    }

    public void MagicAttack() //boss fireball ranged
    {
        if (Time.time > BossManager.boss.bossNextFire)
        {
            BossManager.boss.bossNextFire = Time.time + BossManager.boss.bossFireRate;
            Fire.Play(); //play sound effect 
            GameObject fireProject = Instantiate<GameObject>(BossManager.boss.bFire, BossManager.boss.bFireSpawn.position, BossManager.boss.bFireSpawn.rotation); //instantiate fireball
        }
    }

    void OnTriggerEnter2D(Collider2D other) //if boss hit by player fireball or sword 
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            BossManager.boss.bossHP -= BossManager.boss.magicBase;
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Attack"))
        {
            BossManager.boss.bossHP -= BossManager.boss.attackBase;
            BossManager.boss.sword.gameObject.SetActive(false);
        }
    }
}

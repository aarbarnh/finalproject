﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //camera variables 
    private Transform tf;
    private Vector3 offset;
    public Transform playerTf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        offset = transform.position - playerTf.position; //offset for camera 
    }

    // Update is called once per frame
    void Update()
    {
        tf.position = playerTf.position + offset; //update camera position
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //player variables
    private Transform tf;
    public float moveSpeed = 5.0f;
    public float moveNoise = 5.0f;
    public float turnSpeed = 50.0f;
    public float turnNoise = 5.0f;
    public NoiseMaker noise;

    //audio clips
    public AudioClip pickup;
    public AudioSource pickups;
    public AudioClip effect;
    public AudioSource Magic;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //get all elements, set clips
        noise = GetComponent<NoiseMaker>();
        pickups.clip = pickup;
        Magic.clip = effect;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) //tank controls
            {
                MoveForward();
            }
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                MoveBackward();
            }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                RotateLeft();
            }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                RotateRight();
            }

        if (Input.GetKey(KeyCode.Space) && Time.time > GameManager.game.nextFire) //fireball 
        {
            GameManager.game.nextFire = Time.time + GameManager.game.fireRate;
            Magic.Play();
            GameObject fireProject = Instantiate<GameObject>(GameManager.game.fireball, GameManager.game.fireSpawn.position, GameManager.game.fireSpawn.rotation);
        }

        if (Input.GetKeyDown(KeyCode.R) && Time.time > GameManager.game.attackNext) //melee
        {
            GameManager.game.attackNext = Time.time + GameManager.game.attackRate;
            GameManager.game.sword.gameObject.SetActive(true);
        }

        if(Input.GetKey(KeyCode.F) && Time.time > GameManager.game.healNext) //heal
        {
            GameManager.game.healNext = Time.time + GameManager.game.healRate;
            GameManager.game.Heal();
        }
    }

    void MoveForward() //move forward
    {
        tf.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        if (noise!= null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }
    void MoveBackward() //move backward
    {
        tf.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }
    void RotateLeft() //rotate left
    {
        tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, turnNoise);
        }
    } 
    void RotateRight() //rotate right
    {
        tf.Rotate(0, 0, -(turnSpeed * Time.deltaTime));
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, turnNoise);
        }
    }

    void OnTriggerEnter2D(Collider2D other) //on trigger for pick ups
    {
        if (other.gameObject.CompareTag("Spirit"))
        {
            GameManager.game.pSpirit++;
            GameManager.game.currentAdds--;
            UIManager.ui.spirit.text = GameManager.game.pSpirit.ToString();
            pickups.Play();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Defense"))
        {
            GameManager.game.pDefense++;
            GameManager.game.currentAdds--;
            UIManager.ui.defense.text = GameManager.game.pDefense.ToString();
            pickups.Play();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Attack"))
        {
            GameManager.game.pAttack++;
            GameManager.game.currentAdds--;
            UIManager.ui.attack.text = GameManager.game.pAttack.ToString();
            pickups.Play();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Magic"))
        {
            GameManager.game.pMagic++;
            GameManager.game.currentAdds--;
            UIManager.ui.magic.text = GameManager.game.pMagic.ToString();
            pickups.Play();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("EBullet")) //if hit by enemy bullet
        {
            GameManager.game.TakeDamage();
            Destroy(other.gameObject);
        }
    }
}


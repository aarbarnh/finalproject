﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePartyController : MonoBehaviour
{
    private AISenses senses; //grab senses
    private Transform tf;

    //other party attributes
    public Vector3 homePos;
    public Vector3 goalPos;
    public float giveUpDistance = 5.0f;
    public float closeEnough;
    public float moveSpeed = 2.0f;
    public float turnSpeed = 50.0f;
    public AIStates currentState;

    //FSM for enemy
    public enum AIStates
    {
        Idle,
        LookFor,
        ChasePlayer,
        GoHome
    }

    // Use this for initialization
    void Start()
    {
        //get components and set homePos
        senses = GetComponent<AISenses>();
        tf = GetComponent<Transform>();
        homePos = tf.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.game.bEnemyHP <= 0)
        {
            GameManager.game.bEnemyDead = true;
            Destroy(this.gameObject);
        }
        switch (currentState) //change the enemy's FSM depending on AI senses
        {
            case AIStates.Idle: //idling
                Idle();
                if (senses.HearPlayer(GameManager.game.player)) //if hear, lookfor
                {
                    currentState = AIStates.LookFor;
                }
                if (senses.SeePlayer(GameManager.game.player)) //if see, chase
                {
                    currentState = AIStates.ChasePlayer;
                }
                break;
            case AIStates.LookFor: //looking for 
                LookFor();
                if (senses.SeePlayer(GameManager.game.player)) //if see, chase
                {
                    currentState = AIStates.ChasePlayer;
                }
                if (!senses.HearPlayer(GameManager.game.player)) //if can't hear
                {
                    if (tf.position != homePos) //if not at home pos, go home
                    {
                        currentState = AIStates.GoHome;
                    }
                    else //if at home pos, idle
                    {
                        currentState = AIStates.Idle;
                    }
                }
                break;
            case AIStates.ChasePlayer: //chasing
                ChasePlayer();
                MagicAttack();
                if (!senses.SeePlayer(GameManager.game.player)) //if can't see
                {
                    if (senses.HearPlayer(GameManager.game.player)) //if can hear, look for
                    {
                        currentState = AIStates.LookFor;
                    }
                    else //if can't hear, go home
                    {
                        currentState = AIStates.GoHome;
                    }
                }
                if (Vector3.Distance(tf.position, GameManager.game.player.transform.position) > giveUpDistance) //if player surpasses give up distance 
                {
                    if (senses.HearPlayer(GameManager.game.player)) //if can hear, look for
                    {
                        currentState = AIStates.LookFor;
                    }
                    else //go home if can't hear and out of range
                    {
                        currentState = AIStates.GoHome;
                    }
                }
                break;
            case AIStates.GoHome: //going home
                GoHome();
                if (senses.HearPlayer(GameManager.game.player)) //if can hear, look for
                {
                    currentState = AIStates.LookFor;
                }
                if (senses.SeePlayer(GameManager.game.player)) //if can see, chase
                {
                    currentState = AIStates.ChasePlayer;
                }
                if (Vector3.Distance(tf.position, homePos) <= closeEnough) //if close enough to home pos (1), idle
                {
                    currentState = AIStates.Idle;
                }
                break;
        }
    }

    public void Idle() //idle does nothing 
    {

    }

    public void LookFor() //rotates 
    {
        tf.Rotate(0, 0, turnSpeed * Time.deltaTime);
    }

    public void GoHome() //goes to goalPos which is homePos
    {
        goalPos = homePos;
        MoveTowards(goalPos);
    }

    public void ChasePlayer() //move toward player position
    {
        goalPos = GameManager.game.player.transform.position;
        MoveTowards(goalPos);
    }

    public void MoveTowards(Vector3 target) //how to move towards
    {
        if (Vector3.Distance(tf.position, target) > closeEnough) //if the distance between player and target is greater than close, keep moving towards
        {
            Vector3 vToTarget = target - tf.position;
            tf.up = vToTarget;
            Move(tf.up);
        }
    }

    public void Move(Vector3 direction) //getting the movement for move towards 
    {
        tf.position += (direction.normalized * moveSpeed * Time.deltaTime);
    }

    public void MagicAttack() //magic ranged attack for enemy 
    {
        if (Time.time > GameManager.game.blueNextFire)
        {
            GameManager.game.blueNextFire = Time.time + GameManager.game.enemyFireRate;
            GameObject fireProject = Instantiate<GameObject>(GameManager.game.eFireball, GameManager.game.eFireSpawn.position, GameManager.game.eFireSpawn.rotation);
        }
    }

    void OnTriggerEnter2D(Collider2D other) //if hit by fireball or sword
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            int dmg = GameManager.game.pMagic * GameManager.game.magicBase;
            GameManager.game.bEnemyHP -= dmg;
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Attack"))
        {
            int dmg = GameManager.game.pAttack * GameManager.game.attackBase;
            GameManager.game.bEnemyHP -= dmg;
            GameManager.game.sword.gameObject.SetActive(false);
        }
    }
}

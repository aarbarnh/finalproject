﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eBullet : MonoBehaviour
{
    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        Destroy(this.gameObject, GameManager.game.enemyFireTime); //delayed destroy
    }

    // Update is called once per frame
    void Update()
    {
        tf.Translate(Vector3.up * GameManager.game.enemyBulletSpeed * Time.deltaTime); //movement
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletonBoss : MonoBehaviour
{
    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        Destroy(this.gameObject, BossManager.boss.fireTime); //delayed destroy 
    }

    // Update is called once per frame
    void Update()
    {
        tf.Translate(Vector3.right * BossManager.boss.bulletSpeed * Time.deltaTime); //movement
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager ui; //singleton

    //text elements
    public Text attack;
    public Text defense;
    public Text magic;
    public Text spirit;
    public Text health;

    void Awake()
    {
        if (ui == null)
        {
            ui = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

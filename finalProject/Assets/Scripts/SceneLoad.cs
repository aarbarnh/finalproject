﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    public static SceneLoad sloader;
    // Start is called before the first frame update
    void Start()
    {
        if (sloader == null)
        {
            sloader = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadScene(string scene) //loading a scene by string
    {
        SceneManager.LoadScene(scene);
    }

    public void QuitGame() //quitting game
    {
        Application.Quit();
    }
}

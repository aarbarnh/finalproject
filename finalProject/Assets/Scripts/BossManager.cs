﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    public static BossManager boss; //singleton (game manager of the boss level)

    //win/lose screens
    public GameObject bossScreen;
    public GameObject winScreen;
    public GameObject loseScreen;

    //player variables
    public bool playerDead = false;
    public int playerHP = 50;
    public GameObject player;

    //bullet variables
    public GameObject fireball;
    public Transform fireSpawn;
    public float fireTime = 0.5f;
    public float nextFire = 0.0f;
    public float fireRate = 0.5f;
    public float bulletSpeed = 30.0f;
    public int magicBase = 2;

    //sword variables(attack)
    public GameObject sword;
    public float attackNext = 0.0f;
    public float attackRate = 1.5f;
    public int attackBase = 5;

    //boss variables
    public int bossHP = 50;
    public bool bossDead = false;
    public int bossAtkVal = 10;
    public float bossFireTime = 1.5f;
    public float bossNextFire = 0.0f;
    public float bossFireRate = 0.5f;
    public float bossBulletSpeed = 1.5f;
    public GameObject bFire;
    public Transform bFireSpawn;

    void Awake()
    {
        if (boss == null)
        {
            boss = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHP <= 0)
        {
            playerDead = true;
        }
        if (bossHP <= 0)
        {
            bossDead = true;
        }

        if (playerDead == true) //if player dead, set lose screen
        {
            bossScreen.SetActive(false);
            loseScreen.SetActive(true);
        }
        if (bossDead == true) //if boss dead, set win screen
        {
            bossScreen.SetActive(false);
            winScreen.SetActive(true);
        }

        if (Input.GetKey(KeyCode.Escape)) //quit
        {
            Application.Quit();
        }
    }
}

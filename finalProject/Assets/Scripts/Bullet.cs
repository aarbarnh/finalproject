﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        Destroy(this.gameObject, GameManager.game.fireTime); //delayed destroy 
    }

    // Update is called once per frame
    void Update()
    {
        tf.Translate(Vector3.right * GameManager.game.bulletSpeed * Time.deltaTime); //movement
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //lose screen/game screen
    public GameObject gameScreen;
    public GameObject loseScreen;

    //player variables
    public bool playerDead = false;
    public int playerHP = 50;
    public int maxHP = 50;
    public GameObject player;
    public int pDefense = 0;
    public int pAttack = 1;
    public int pMagic = 1;
    public int pSpirit = 1;

    //bullet variables
    public GameObject fireball;
    public Transform fireSpawn;
    public float fireTime = 0.5f;
    public float nextFire = 0.0f;
    public float fireRate = 0.5f;
    public float bulletSpeed = 30.0f;
    public int magicBase = 2;

    //sword variables(attack)
    public GameObject sword;
    public float attackNext = 0.0f;
    public float attackRate = 1.5f;
    public int attackBase = 5;

    //healing variables
    public float healNext = 0.0f;
    public float healRate = 15.0f;
    public int healValue = 5;

    //enemy/other party variables
    public int bEnemyHP = 25;
    public int gEnemyHP = 25;
    public bool bEnemyDead = false;
    public bool gEnemyDead = false;
    public int eAttackVal = 5;
    public float enemyFireTime = 1.0f;
    public float blueNextFire = 0.0f;
    public float greenNextFire = 0.0f;
    public float enemyFireRate = 1.0f;
    public float enemyBulletSpeed = 2.5f;
    public GameObject eFireball;
    public GameObject eIceBolt;
    public Transform eFireSpawn;
    public Transform eIceSpawn;

    //power up/ add ons variables 
    public List<GameObject> addOns;
    private int maxAdds = 5;
    public int currentAdds = 0;
    public Transform[] PowerPoints;
    public float addOnRate = 10.0f;
    public float addOnNext = 5.0f;

    //singleton
    public static GameManager game;
    void Awake()
    {
        if (game == null)
        {
            game = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (currentAdds < maxAdds && Time.time > addOnNext) 
        {
            addOnNext = Time.time + addOnRate;
            GameObject newAdd = addOns[Random.Range(0, addOns.Count)]; //get the add on by random
            GameObject addInstance = Instantiate(newAdd, PowerPoints[Random.Range(0,PowerPoints.Length)].transform.position, Quaternion.identity); //spawn the powerup/add on
            currentAdds++;
        }

        if (playerHP <= 0) //if player dies
        {
            playerDead = true;
        }

        if(playerDead == true) //if player dead, set lose screen
        {
            gameScreen.SetActive(false);
            loseScreen.SetActive(true);
        }
        if (gEnemyDead && bEnemyDead) //if enemies are both dead, load boss scene
        {
            SceneLoad.sloader.LoadScene("Boss");
        }

        if (Input.GetKey(KeyCode.Escape)) //quit game
        {
            Application.Quit();
        }
    }

    public void Heal() //player heal function
    {
        if (playerHP < maxHP)
        {
            int healAmount = healValue * pSpirit;
            if (playerHP + healAmount > maxHP) //update ui
            {
                playerHP = maxHP;
                UIManager.ui.health.text = playerHP.ToString() + "/50";
            }
            else
            {
                playerHP += healAmount;
                UIManager.ui.health.text = playerHP.ToString() + "/50";
            }
        }
    }

    public void TakeDamage() //player take damage
    {
        int trueDmg = eAttackVal - pDefense;
        playerHP -= trueDmg;
        UIManager.ui.health.text = playerHP.ToString() + "/50"; //update ui
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISenses : MonoBehaviour
{
    //AI data view/hear
    public float viewDistance = 10.0f;
    public float fieldOfView = 60.0f;
    public float hearDistance = 2.0f;
    const float DEBUG_ANGLE_DISTANCE = 2.0f;
    const float DEGREES_TO_RADIANS = Mathf.PI / 180.0f;

    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool HearPlayer(GameObject target) //hearing the player 
    {
        NoiseMaker targetNoise = target.GetComponent<NoiseMaker>(); //get the player's noisemaker 
        if (targetNoise == null) //if none, can't hear 
        {
            return false;
        }

        Transform targetPos = target.GetComponent<Transform>(); //get player's transform
        if (Vector3.Distance(targetPos.position, tf.position) <= targetNoise.currentVolume * hearDistance) //gets distance from the chicken to the player, compares to hear distance 
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SeePlayer(GameObject target) //see player function 
    {
        Collider2D targetCollider = target.GetComponent<Collider2D>(); //get player's collider 
        if (targetCollider == null) //if none, can't see 
        {
            return false;
        }

        Transform targetPos = target.GetComponent<Transform>(); //get the transform of player
        Vector3 vToTarget = targetPos.position - tf.position; //get the vector to player from current pos
        vToTarget.Normalize();

        if (Vector3.Angle(vToTarget, tf.up) >= fieldOfView) //if the angle is more than the FOV, can't see
        {
            return false;
        }

        RaycastHit2D hitCast = Physics2D.Raycast(tf.position, vToTarget, viewDistance); //raycast to player from chicken

        if (hitCast.collider == null) //if no hit, can't see (didn't hit any collider)
        {
            return false;
        }

        if (hitCast.collider == targetCollider) //if hit and its the player's collider, can see
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}
